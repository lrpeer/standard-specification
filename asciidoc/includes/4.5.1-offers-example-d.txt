<Offers>
    <Offer>
        <OfferRules>
            <OfferRule>
                <Occupancy AgeQualifyingCode="10" MinAge="16" />
                <Occupancy AgeQualifyingCode="8" />
            </OfferRule>
        </OfferRules>
    </Offer>
    <Offer>
        <Discount Percent="100" NightsRequired="4" NightsDiscounted="1" />
    </Offer>
</Offers>
