<Supplements>

<Supplement InvType="EXTRA"
            InvCode="0x539"
            AddToBasicRateIndicator="true"
            MandatoryIndicator="true"
            ChargeTypeCode="18">
    <Description Name="title">
        <Text TextFormat="PlainText" Language="de">Endreinigung</Text>
        <Text TextFormat="PlainText"  Language="it">Pulizia finale</Text>
    </Description>
    <Description Name="intro">
        <Text TextFormat="PlainText" Language="de">
            Die Endreinigung lorem ipsum dolor sit amet.
        </Text>
        <Text  TextFormat="PlainText" Language="it">
            La pulizia finale lorem ipsum dolor sit amet.
       </Text>
    </Description>
</Supplement>

<Supplement InvType="EXTRA"
            InvCode="0x539"
            Amount="20"
            Start="2014-10-01"
            End="2014-10-11">
</Supplement>

</Supplements>
