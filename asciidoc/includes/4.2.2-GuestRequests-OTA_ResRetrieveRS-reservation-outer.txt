<?xml version="1.0" encoding="UTF-8"?>

<OTA_ResRetrieveRS
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.opentravel.org/OTA/2003/05"
        xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_ResRetrieveRS.xsd"
        Version="7.000">

    <Success/>

    <ReservationsList>

        <HotelReservation CreateDateTime="2012-03-21T15:00:00+01:00"
                          ResStatus="Reserved">

              <!-- Type 14 -> Reservation -->
            <UniqueID Type="14" ID="6b34fe24ac2ff810"/>

             <RoomStays>      <!-- stays, see below -->                    </RoomStays>

             <ResGuests>      <!-- customer data, see below -->            </ResGuests>

             <ResGlobalInfo>  <!-- additional booking data, see below -->  </ResGlobalInfo>

        </HotelReservation>

    </ReservationsList>

</OTA_ResRetrieveRS>
