[[anchor-4]]
== 4. Data exchange actions

These actions allow the actual exchange of data between client and server.

For data exchange actions, both parameters ([request_param]`action` and [request_param]`request`) are mandatory.

The value of the [request_param]`request` parameter (sent by the client) and the server response are XML documents following OTA2015A and using the XML root elements specified in the following table:

[cols="3,3,4,4",options="header"]
|===
| known as (since) | usage | parameter [request_param]`action` (string) | parameter [request_param]`request` and server response (XML documents)

a[freerooms_bkg]| [small]#*FreeRooms* (since 2011-11)#
a[freerooms_bkg]| [small]#a client sends room availability notifications to a server#
a[freerooms_bkg]| [small]#`OTA_HotelInvCountNotif:FreeRooms`#
a[freerooms_bkg]| [small]#`OTA_HotelInvCountNotifRQ` `OTA_HotelInvCountNotifRS`#

a[guestrequests_bkg]| [small]#*GuestRequests* (since 2012-05)#
a[guestrequests_bkg]| [small]#a client sends a request to receive requests for a quote or booking requests from the server#
a[guestrequests_bkg]| [small]#`OTA_Read:GuestRequests`#
a[guestrequests_bkg]| [small]#`OTA_ReadRQ` `OTA_ResRetrieveRS`#

a[guestrequests_bkg]| [small]#*GuestRequests (Push)* (since 2018-10)#
a[guestrequests_bkg]| [small]#a client sends requests for a quote or booking requests to the server#
a[guestrequests_bkg]| [small]#`OTA_HotelResNotif:GuestRequests`#
a[guestrequests_bkg]| [small]#`OTA_HotelResNotifRQ` `OTA_HotelResNotifRS`#

a[guestrequests_bkg]| [small]#*GuestRequests Status Update (Push)* (since 2022-10)#
a[guestrequests_bkg]| [small]#a client sends status updates for a quote or booking requests to the server#
a[guestrequests_bkg]| [small]#`OTA_HotelResNotif:GuestRequests_StatusUpdate`#
a[guestrequests_bkg]| [small]#`OTA_HotelResNotifRQ` `OTA_HotelResNotifRS`#

a[guestrequests_bkg]| [small]#*GuestRequests/Acknowledgments* (since 2014-04)#
a[guestrequests_bkg]| [small]#a client acknowledges the requests it has received#
a[guestrequests_bkg]| [small]#`OTA_NotifReport:GuestRequests`#
a[guestrequests_bkg]| [small]#`OTA_NotifReportRQ` `OTA_NotifReportRS`#

a[simplepackages_bkg]| [small]#*SimplePackages* (2012-05 until 2015-07b)#
3+^a[simplepackages_bkg]| [small]#this action has been removed in version 2017-10#

a[inventory_bkg]| [small]#*Inventory/Basic (Push)* (since 2015-07)#
a[inventory_bkg]| [small]#a client sends room category information and room lists#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveContentNotif:Inventory`#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveContentNotifRQ` `OTA_HotelDescriptiveContentNotifRS`#

a[inventory_bkg]| [small]#*Inventory/Basic (Pull)* (since 2017-10)#
a[inventory_bkg]| [small]#a client requests room category information and room lists#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveInfo:Inventory`#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveInfoRQ` `OTA_HotelDescriptiveInfoRS`#

a[inventory_bkg]| [small]#*Inventory/HotelInfo (Push)* (since 2015-07)#
a[inventory_bkg]| [small]#a client sends additional descriptive content regarding properties#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveContentNotif:Info`#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveContentNotifRQ` `OTA_HotelDescriptiveContentNotifRS`#

a[inventory_bkg]| [small]#*Inventory/HotelInfo (Pull)* (since 2017-10)#
a[inventory_bkg]| [small]#a client requests additional descriptive content regarding properties#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveInfo:Info`#
a[inventory_bkg]| [small]#`OTA_HotelDescriptiveInfoRQ` `OTA_HotelDescriptiveInfoRS`#

a[rateplans_bkg]| [small]#*RatePlans* (since 2014-04)#
a[rateplans_bkg]| [small]#a client sends information about rate plans with prices and booking rules#
a[rateplans_bkg]| [small]#`OTA_HotelRatePlanNotif:RatePlans`#
a[rateplans_bkg]| [small]#`OTA_HotelRatePlanNotifRQ` `OTA_HotelRatePlanNotifRS`#

a[baserates_bkg]| [small]#*BaseRates* (since 2017-10)#
a[baserates_bkg]| [small]#a client requests information about rate plans#
a[baserates_bkg]| [small]#`OTA_HotelRatePlan:BaseRates`#
a[baserates_bkg]| [small]#`OTA_HotelRatePlanRQ` `OTA_HotelRatePlanRS`#

a[activity_bkg]| [small]#*Activities* (since 2020-10)#
a[activity_bkg]| [small]#a client requests information about hotel activities#
a[activity_bkg]| [small]#`OTA_HotelPostEventNotif:EventReports`#
a[activity_bkg]| [small]#`OTA_HotelPostEventNotifRQ` `OTA_HotelPostEventNotifRS`#
|===

{AlpineBitsR} *requires* all XML documents to be encoded in *UTF-8*.

Caution must be taken when dealing with special and unusual characters (e.g. emojis, kanjis,...).
Their use is increasing and where the user is allowed to enter arbitrary text (mostly from mobile devices) they could be inserted and sent through APIs.
{AlpineBitsR} and XML format can handle any character without issue but if unexpected, they might lead to unpredictable results, particularly on unprepared applications.
Usually, supporting Unicode or UTF-8 encoding prevents most potential serious issues, but some unpleasant situations can still occur; for example:

* Incorrect visualization of certain characters might create confusion in text comprehension or in text formatting.
* Unicode characters bigger than 16 bits might not be supported yet by every system and can also cause alterations in stored data.
* Ambiguity between characters and their composite version (e.g. characters with diacritical marks) might create issues with some algorithms (e.g. searches, comparisons).

Therefore, it is a good practice to parse the data received with {AlpineBitsR} for special characters prior to storing it. In worst cases this will lead only to a validation error rather than more unpleasant situations.
More in general, it is advised to ensure the necessary checks in order to prepare its own application to receive special characters.

The business logic of an {AlpineBitsR} server, i.e. how the server processes and stores the information it receives is implementation-specific.

The format of the requests and responses is, however, exactly specified.

First of all the requests and responses *must* validate against the OTA2015A schema.

Since OTA is very flexible regarding mandatory / optional elements, {AlpineBitsR} adds extra requirements about exactly which elements and attributes are required in a request.

If these are not present, a server’s business logic is bound to fail and will return a response indicating an error even though the request is valid OTA2015A.

OTA2015A, for instance allows `OTA_HotelInvCountNotifRQ` requests that do not indicate the hotel, this might make perfect sense in some context, but an {AlpineBitsR} server will return an error if that information is missing from the request.

To aid developers, two {AlpineBitsR} schema files (one XML schema file and one Relax NG schema file) are provided as an integral part of the specification in the {AlpineBitsR} documentation kit.

The Relax NG file is somewhat stricter than the XML schema file, as RelaxNG is intrinsically more powerful in expressing constraints that express how elements and attributes depend on each other.

Both {AlpineBitsR} schema files are stricter than OTA2015A in the sense that all documents that validate against {AlpineBitsR} will also validate against OTA2015A, but not vice versa.

The {AlpineBitsR} documentation kit also provides a sample file for each of the request and response documents.

The latest {AlpineBitsR} documentation kit for each protocol version is available from the official {AlpineBitsR} website.

Even though the XML snippets and sample files express prices in EUR, in {AlpineBitsR} all currency codes from the ISO 4217 are allowed. It is important that clients and servers that exchange data with each other use the same currency code.
In the event that a server receives unsupported currencies in a message, it must reply to the client with a warning outcome. Likewise, a client that receives unsupported currencies should discard the whole message and notify the server in some way.

=== On copyright and license of exchanged contents

Many AlpineBits messages allow to transmit URLs to multimedia objects (mostly images), and whenever possible suggest the explicit identification of the copyright holder. However, in order to know its rights, the receiving end must also be aware of the licensing terms attached to these multimedia resource. Unfortunately there is no place for this information inside the XML messages, hence some other means of conveying it is suggested below as a best practice.

* The publisher of the multimedia object could add some metadata (e.g. EXIF in case of pictures) that contains the licensing information. To this end, AlpineBits recommends the utilization of a standard like the ``IPTC`` specification. The receiving end *should not* ignore nor remove this metadata and *should* also include them in a derived work (e.g. thumbnails or resized representations).
* The publisher of the multimedia object could add some metadata to the HTTP response that serves the multimedia content, to this end AlpineBits recommends the usage of ``X-AlpineBits-License`` and ``X-AlpineBits-CopyrightHolder`` headers. The receiving end *should not* ignore these headers and *should* consider also any derived work to be under the same constraints.

Please note that these approaches are not mutually exclusive. Furthermore it is recommended to provide as much information as possible within each data exchange.

<<<
include::041-freerooms.adoc[]

<<<
include::042-guestrequests.adoc[]

<<<
include::043-simplepackages.adoc[]

<<<
include::044-inventory.adoc[]

<<<
include::045-rateplans.adoc[]

<<<
include::046-baserates.adoc[]

<<<
include::047-activity.adoc[]
